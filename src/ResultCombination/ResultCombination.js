import React from 'react';

const ResultCombination = (props) => (
    <div>
        <h3>
            {props.result}
        </h3>
    </div>
);

export default ResultCombination;