import React, {Component} from 'react';

import './App.css';
import './cards.css';
import Card from "./Card/Card";
import Buttons from "./Buttons/Buttons";
import CardDeck from './cardDeck.js';
import Combination from './combination';
import ResultCombination from "./ResultCombination/ResultCombination";


class App extends Component {
    combinations = new Combination();
    deck;
    state = {
        cards: [],
        replacementCards: [],
        isPlayed: false
    };
    changeCards = () => {
        if (!this.state.isPlayed && this.state.replacementCards.length!==0) {
            let cards = [...this.state.cards];
            for (let i = 0; i < this.state.replacementCards.length; i++) {
                cards[this.state.replacementCards[i].index] = this.deck.getCard();
            }
            this.setState({cards, replacementCards: [], isPlayed: true});
        }

    };
    createDeck = () => {
        this.deck = new CardDeck();
        const cards = this.deck.getCards(5);
        document.getElementById("closeCards").innerText = " ";
        this.setState({cards,replacementCards: [], isPlayed: false});
    };

    hideMap = (id) => {
        let replacementCards = [...this.state.replacementCards];
        const index = this.state.cards.findIndex(t => t.id === id);
        let card = document.getElementById(id);
        if (card.classList.contains("back")) {
            card.classList.remove("back");
            card.classList.add(this.state.cards[index].suit, this.state.cards[index].rank);
            const index2 = replacementCards.findIndex(i => i.index === index);
            replacementCards.splice(index2, 1);
        }
        else {
            card.classList.remove(this.state.cards[index].suit, this.state.cards[index].rank);
            card.classList.add("back");
            const c = {index: index};
            replacementCards.push(c);
        }
        this.setState({replacementCards});
    };
    render() {
        return (
            <div className="App">
                <ResultCombination result={this.combinations.serchCombinations(this.state.cards)}/>
                <div className="playingCards">
                    <div id="closeCards">
                        <div className="card back">*</div>
                        <div className="card back">*</div>
                        <div className="card back">*</div>
                        <div className="card back">*</div>
                        <div className="card back">*</div>
                    </div>
                    {this.state.cards.map(card =>
                        <Card
                            key={card.id} suit={card.suit} rank={card.rank} cardClick={() => this.hideMap(card.id)}
                        />
                    )}
                </div>
                <Buttons clickChange={this.changeCards} onClick={this.createDeck}/>
            </div>
        );
    }
}

export default App;
