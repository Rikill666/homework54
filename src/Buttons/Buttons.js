import React from 'react';

const Buttons = (props) => (
    <div>
        <button onClick={props.onClick}>Раздать карты</button>
        <button onClick={props.clickChange}  id="changeButton">Заменить выбранные карты</button>
    </div>
);

export default Buttons;