import React from 'react';

const Card = (props) => {
    let className = "card rank-" + props.rank + " " + props.suit;
    let suit;
    switch(props.suit){
        case 'hearts':
            suit = "♥";
            break;
        case 'spades':
            suit = "♠";
            break;
        case 'diams':
            suit = "♦";
            break;
        case 'clubs':
            suit = "♣";
            break;
        default:
            suit = "♥";
    }
    return (
        <div onClick={props.cardClick} className={className} id={props.suit + props.rank}>
            <span className="rank">{props.rank.toUpperCase()}</span>
            <span className="suit">{suit}</span>
        </div>
    )
};

export default Card;