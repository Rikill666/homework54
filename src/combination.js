class Combination {
    cardsAtFaceValue = [];
    ranksNumbers = {
        '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'j': 11, 'q': 12, 'k': 13, 'a': 14
    };

    serchCombinations(fiveCards) {
        this.cardsAtFaceValue = [];
        let countSuit = 0;
        let suit = "";
        let streetCount = 0;
        for (let i = 0; i < fiveCards.length; i++) {
            let duplicateCardIndex = this.cardsAtFaceValue.findIndex(s => s.name === fiveCards[i].rank);
            if (i === 0) {
                suit = fiveCards[i].suit;
                countSuit++;
            }
            else {
                if (fiveCards[i].suit === suit) {
                    countSuit++;
                }
            }
            if (duplicateCardIndex === -1) {
                const card = {name: fiveCards[i].rank, card: fiveCards[i], count: 1};
                this.cardsAtFaceValue.push(card);
            }
            else {
                this.cardsAtFaceValue[duplicateCardIndex].count++;
            }
        }
        this.cardsAtFaceValue = this.cardsAtFaceValue.sort((a, b) => this.ranksNumbers[a.card.rank] - this.ranksNumbers[b.card.rank]);
        if(this.cardsAtFaceValue.length === 5){
          for(let i = 0; i < this.cardsAtFaceValue.length; i++){
              if(i!==4 && this.ranksNumbers[this.cardsAtFaceValue[i+1].card.rank] - this.ranksNumbers[this.cardsAtFaceValue[i].card.rank] === 1){
                  streetCount++;
              }
          }
        }
        if(streetCount === 4 && countSuit === 5){
            return "Street Flash on " + this.cardsAtFaceValue[this.cardsAtFaceValue.length-1].card.rank.toUpperCase() + ": "+ suit;
        }
        else if(streetCount === 4){
            return "Street on " + this.cardsAtFaceValue[this.cardsAtFaceValue.length-1].card.rank.toUpperCase();
        }
        else if (countSuit === 5) {
            return "Flash: " + suit;
        }
        else {
            let kare = this.cardsAtFaceValue.findIndex(c => c.count === 4);
            let set = this.cardsAtFaceValue.findIndex(c => c.count === 3);
            let countPairs = this.cardsAtFaceValue.filter(c => c.count === 2);

            if(kare !== -1){
                return "Four of a kind: " + this.cardsAtFaceValue[kare].card.rank.toUpperCase();
            }
            else if (set !== -1 && countPairs.length === 1){
                return "Full House: " + this.cardsAtFaceValue[set].card.rank.toUpperCase() + " and " + countPairs[0].card.rank.toUpperCase();
            }
            else if (set !== -1) {
                return "Set: " + this.cardsAtFaceValue[set].card.rank.toUpperCase();
            }
            else {

                if (countPairs.length === 2) {
                    return "Two Pairs: " + countPairs[0].card.rank.toUpperCase() + " and " + countPairs[1].card.rank.toUpperCase();
                }
                else if(countPairs.length === 1) {
                    return "Pairs: " + countPairs[0].card.rank.toUpperCase();
                }
                else if(this.cardsAtFaceValue.length !== 0){
                    return "High card: " + this.cardsAtFaceValue[this.cardsAtFaceValue.length-1].card.rank.toUpperCase();
                }
                else{
                    return "No combinations";
                }
            }
        }
    }
}
export default Combination;